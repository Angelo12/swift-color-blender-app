//
//  ViewController.swift
//  ColorBlender
//
//  Created by Angelo J Gomez Franzin on 2/5/15.
//  Copyright (c) 2015 agomez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	let dL1 : CGFloat = 1.25;
	let dL2 : CGFloat = 1.50;
	
	let dD1 : CGFloat = 0.75;
	let dD2 : CGFloat = 0.50;
	
	/* Colors */
	@IBOutlet weak var firstColorView: UIView!
	@IBOutlet weak var secondColorView: UIView!
	
	/* Sliders */
	@IBOutlet weak var redSlider: UISlider!
	@IBOutlet weak var greenSlider: UISlider!
	@IBOutlet weak var blueSlider: UISlider!
	
	/* Text and Hex */
	@IBOutlet weak var redTextView: UITextField!
	@IBOutlet weak var greenTextView: UITextField!
	@IBOutlet weak var blueTextView: UITextField!
	
	@IBOutlet weak var hexText: UILabel!
	
	/* Blend View Outlets */
	@IBOutlet var blendViewCollection: [UIView]!
	
	@IBOutlet var blendViewCollection_L1: [UIView]!
	
	@IBOutlet var blendViewCollection_L2: [UIView]!
	
	@IBOutlet var blendViewCollection_D1: [UIView]!
	
	@IBOutlet var blendViewCollection_D2: [UIView]!

	
	
	var currentActiveView: UIView?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.vr
		
		/* Add gesture recognizers */
		/* Each View needs a unique recognizer */
		
		
		var tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
		firstColorView.addGestureRecognizer(tapGesture);
		TapGestureRecognized(tapGesture);
		
		
		tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
		secondColorView.addGestureRecognizer(tapGesture);
		
		
		
		//Add Tap Gesture recognizers to blended Views //
		
		for var i = 0; i < blendViewCollection.count; ++i {
			
			tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
			blendViewCollection[i].addGestureRecognizer(tapGesture);
			
			tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
			blendViewCollection_D1[i].addGestureRecognizer(tapGesture);
			
			tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
			blendViewCollection_L1[i].addGestureRecognizer(tapGesture);
			
			tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
			blendViewCollection_D2[i].addGestureRecognizer(tapGesture);
			
			tapGesture = UITapGestureRecognizer(target: self, action: "TapGestureRecognized:");
			blendViewCollection_L2[i].addGestureRecognizer(tapGesture);
		}
		
		
		
		
		
		updateBlendedViews();
		
	}
	
	func TapGestureRecognized(sender: UITapGestureRecognizer) {
		
		
		currentActiveView?.layer.borderWidth = 0;
		
		currentActiveView = sender.view;
		
		/* Adjust Slider, Hex Label, and RGB text fields to reflect current selected*/
		
		let intensity = currentActiveView!.layer.backgroundColor.RGBA();
		
		currentActiveView?.layer.borderColor = UIColor.whiteColor().CGColor;
		currentActiveView?.layer.borderWidth = 3.0;
		
		
		redSlider.value = Float(intensity.red);
		greenSlider.value = Float(intensity.green);
		blueSlider.value = Float(intensity.blue);
		
		
		if currentActiveView?.tag == 0 {
			redSlider.enabled =  false;
			greenSlider.enabled =  false;
			blueSlider.enabled =  false;
		} else {
			redSlider.enabled =  true;
			greenSlider.enabled =  true;
			blueSlider.enabled =  true;
		}
		
		
		
		updateHexAndViews(intensity);
	}
	
	
	@IBAction func valueChanged(sender: UISlider)
	{
		
		if let intensities = currentActiveView?.layer.backgroundColor.RGBA() {
			
			switch sender.tag {
				
			case 10:
				currentActiveView?.layer.backgroundColor = UIColor(red: CGFloat(sender.value), green: intensities.green, blue: intensities.blue, alpha: intensities.alpha).CGColor;
				redTextView.text = "\(Int(255*sender.value))";
			case 20:
				currentActiveView?.layer.backgroundColor = UIColor(red: intensities.red, green: CGFloat(sender.value), blue: intensities.blue, alpha: intensities.alpha).CGColor;
				greenTextView.text = "\(Int(255*sender.value))";
			case 30:
				currentActiveView?.layer.backgroundColor = UIColor(red: intensities.red, green: intensities.green, blue: CGFloat(sender.value), alpha: intensities.alpha).CGColor;
				blueTextView.text = "\(Int(255*sender.value))";
			default:
				print("BlaBla")
				
			}
			updateBlendedViews();
			updateHexAndViews(intensities);
		}
	}
	
	
	func updateHexAndViews(intensity: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)) {
		let redInt = Int(255*intensity.red);
		let greenInt = Int(255*intensity.green);
		let blueInt = Int(255*intensity.blue);
		
		redTextView.text = "\(redInt)";
		greenTextView.text = "\(greenInt)";
		blueTextView.text = "\(blueInt)";
		
		hexText.text = currentActiveView?.layer.backgroundColor.hexColor();
	}
	
	func updateBlendedViews() {
		/* Get color values from main color views */
		let color1 = firstColorView.layer.backgroundColor.RGBA();
		let color2 = secondColorView.layer.backgroundColor.RGBA();
		
		
		/* Calculate the Delta(change in value) for each blendedView */
		let redDelta = (color1.red - color2.red) / 8;
		let greenDelta = (color1.green - color2.green) / 8;
		let blueDelta = (color1.blue - color2.blue) / 8;
		
		/* Loop throud and change blend views colors*/
		var newRed : CGFloat = 0;
		var newGreen : CGFloat = 0;
		var newBlue : CGFloat = 0;
		
		for var i = 0; i < blendViewCollection.count; ++i {
			
			newRed = color1.red - redDelta * CGFloat(i);
			newGreen = color1.green - greenDelta * CGFloat(i);
			newBlue = color1.blue - blueDelta * CGFloat(i);
			
			blendViewCollection[i].layer.backgroundColor = UIColor(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0).CGColor;
			
			blendViewCollection_L1[i].layer.backgroundColor = UIColor(red: newRed*dL1, green: newGreen*dL1, blue: newBlue*dL1, alpha: 1.0).CGColor;
			blendViewCollection_D1[i].layer.backgroundColor = UIColor(red: newRed*dD1, green: newGreen*dD1, blue: newBlue*dD1, alpha: 1.0).CGColor;
			blendViewCollection_L2[i].layer.backgroundColor = UIColor(red: newRed*dL2, green: newGreen*dL2, blue: newBlue*dL2, alpha: 1.0).CGColor;
			blendViewCollection_D2[i].layer.backgroundColor = UIColor(red: newRed*dD2, green: newGreen*dD2, blue: newBlue*dD2, alpha: 1.0).CGColor;
		}
		
	}
	
	
}

