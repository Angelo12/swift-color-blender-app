//
//  ColorExtensions.swift
//  ColorBlender
//
//  Created by Angelo J Gomez Franzin on 2/12/15.
//  Copyright (c) 2015 agomez. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
	
	func RGBA() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat){
		
		//Basically an array of CGFloats
		let intensities = CGColorGetComponents(self.CGColor);
		
		//Return the Tuple
		return (intensities[0], intensities[1], intensities[2], intensities[3]);
		
	}
	
	func hexColor() -> String {
		let intensities = CGColorGetComponents(self.CGColor);
		
		let redInt = Int(255*intensities[0]);
		let greenInt = Int(255*intensities[1]);
		let blueInt = Int(255*intensities[2]);
		
		let redHex = String(NSString(format:"%2X", redInt));
		let greenHex = String(NSString(format:"%2X", greenInt));
		let blueHex = String(NSString(format:"%2X", blueInt));
		
		var newString = "#";
		newString += (redInt <= 9 ? "0\(redHex)" : "\(redHex)");
		newString += (greenInt <= 9 ? "0\(greenHex)" : "\(greenHex)");
		newString += (blueInt <= 9 ? "0\(blueHex)" : "\(blueHex)");
		
		return newString;
	}
	
}
extension CGColor {
	
	func RGBA() -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat){
		
		//Basically an array of CGFloats
		let intensities = CGColorGetComponents(self);
		
		//Return the Tuple
		return (intensities[0], intensities[1], intensities[2], intensities[3]);
		
	}
	
	func hexColor() -> String {
		let intensities = CGColorGetComponents(self);
		
		let redInt = Int(255*intensities[0]);
		let greenInt = Int(255*intensities[1]);
		let blueInt = Int(255*intensities[2]);
		
		var redHex = String(NSString(format:"%2X", redInt));
		var greenHex = String(NSString(format:"%2X", greenInt));
		var blueHex = String(NSString(format:"%2X", blueInt));
		
		if redInt <= 15 {
			redHex.removeAtIndex(redHex.startIndex);
			redHex.insert("0", atIndex: redHex.startIndex);
		}
		if greenInt <= 15 {
			greenHex.removeAtIndex(greenHex.startIndex);
			greenHex.insert("0", atIndex: greenHex.startIndex);
		}
		if blueInt <= 15 {
			blueHex.removeAtIndex(blueHex.startIndex);
			blueHex.insert("0", atIndex: blueHex.startIndex);
		}
		
		var newString = "#";
		newString += redHex
		newString += greenHex
		newString += blueHex
		
		return newString;
	}
}